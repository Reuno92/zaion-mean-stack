import { Component } from '@angular/core';

@Component({
  selector: 'zaion-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'client';
}
