import { Component } from '@angular/core';

@Component({
  selector: 'zaion-mean-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {}
